<?php

namespace App\Http\Controllers\api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class authController extends Controller
{
    public function create(Request $request)
    {
        $return['error'] = true;
        $return['msg'] = '';
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'type' => 'required',
            'password' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['type'] = $request->type;
        $data['photo'] = $request->photo;
        $data['bio'] = $request->bio;
        $data['password'] = bcrypt($request->password);
        User::create($data);
        $return['error'] = false;
        $return['msg'] = 'user add successfully';
        return $return;
    }

    public function user()
    {
        return User::all();
    }
}
